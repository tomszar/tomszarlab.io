---
title: "Robust genome-wide ancestry inference for heterogeneous datasets: Illustrated using the 1,000 genome project with 3D facial images"
date: 2020-07-16
doi: https://doi.org/10/gg668r
journal: Scientific Reports
draft: false
---
