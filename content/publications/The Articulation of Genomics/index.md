---
title: "The Articulation of Genomics, Mestizaje, and Indigenous Identities in Chile: A Case Study of the Social Implications of Genomic Research in Light of Current Research Practices"
date: 2022-03-03
doi: https://doi.org/10.3389/fgene.2022.817318
journal: Frontiers in Genetics
draft: false
---
