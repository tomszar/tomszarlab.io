---
title: "The Trotter collection: A review of Mildred Trotter's hair research and an update for studies of human variation"
date: 2024-04-06
doi: https://doi.org/10.1002/ajpa.24930
journal: American Journal of Biological Anthropology
draft: false
---
