---
title: "Differential effects of environmental exposures on clinically relevant endophenotypes between sexes"
date: 2024-09-13
doi: https://doi.org/10.1038/s41598-024-72180-x
journal: Scientific Reports
draft: false
---
