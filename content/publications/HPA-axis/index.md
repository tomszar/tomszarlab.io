---
title: "Hypothalamic-pituitary-adrenal axis attenuation and obesity risk in sexually abused females"
date: 2021-07-01
doi: https://doi.org/10/gj2cdc
journal: Psychoneuroendocrinology
draft: false
---
